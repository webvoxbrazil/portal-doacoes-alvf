.bg-blue-dark {
  background-color: #283c5a !important;
}
.text-blue-light-2 {
  color: #7193d0 !important;
}
.bg-blue-dark-3 {
  background-color: #2a3f96 !important;
}
.bg-blue-dark-4 {
  background-color: #233885 !important;
}
.bg-blue-dark-5 {
  background-color: #213655 !important;
}
address,
li,
ol,
ul {
  list-style: none;
  margin: 0;
  padding: 0;
}
