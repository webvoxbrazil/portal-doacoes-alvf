<?php
// Valida os Dados do AJAX
$data = $_POST['data'];
$nome = (sanitize_text_field($data['name']));
$email = sanitize_email($data['email']);
$document_type = sanitize_text_field($data['document_type']);
$document_number = sanitize_text_field($data['document']);
$gender = sanitize_text_field($data['gender']);
$phone_area_code = sanitize_text_field($data['phone_area_code']);
$phone_number = sanitize_text_field($data['phone_number']);
$project_name = sanitize_text_field($data['project_name']);
$project_id = sanitize_text_field($data['project_id']);
// 
// 
// Vadida dos dados para Criar o Customer
$customer = json_encode([
  "name" => ($nome) ? $nome : "Fulano de Tal",
  "email" => ($email) ? $email :  "usuario@gmail.com",
  "code" => "null",
  "document_type" => ($document_type) ? $document_type : "CPF",
  "document" => ($document_number) ? $document_number : "57453743060",
  "type" => "individual",
  "gender" => ($gender) ? $gender : "male",
  "phones" => [
    "mobile_phone" => [
      "country_code" => "55",
      "area_code" => ($phone_area_code) ? $phone_area_code : "49",
      "number" => ($phone_number) ? $phone_number : "999999999",
    ]
  ]
], true);

$basicAuthUserName = get_field('secret_key', $project_id);
// $basicAuthUserName =  $project_key;
require __DIR__ . '/pagarme-api.php';


// // Exemplo de customer
// $doador = array(
//   "name" => "Tony Stark",
//   "code" => "null",
//   "document" => "06444485983",
//   "type" => "individual",
//   "email" => "web@voxbrazil.com.br",
//   "gender" => "male",
//   "document_type" => "CPF",
//   "phones" => [
//     "mobile_phone" => [
//       "country_code" => "55",
//       "area_code" => "49",
//       "number" => "984198753"
//     ]
//   ]
// );
