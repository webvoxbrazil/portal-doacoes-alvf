<?php
require(__DIR__ . '/vendor/autoload.php');
//  public_key -> pk_test_3pZl90SbQfBJ2EnB
// secret_key -> sk_test_mypPrJasBmHK4evg

// $basicAuthUserName = 'sk_test_mypPrJasBmHK4evg'; // Secret Key
$authorization = base64_encode($basicAuthUserName . ':""');  // Cria o basic Authorization.


$client = new \GuzzleHttp\Client();


// Requisição para a API do Pagar.me
$response = $client->request('POST', 'https://api.pagar.me/core/v5/orders/', [
    'body' => '{
    "items": [
        {
            "amount": 1000,
            "description": "' . $project_name . '",
            "quantity": 1,
            "code": "' . $project_id . '"
        }
    ],
    "customer": ' . $customer . ',
    "payments": [
        {
            "payment_method": "checkout",
            "amount": 1000,
            "checkout": {
                "expires_in": 60,
                "customer_editable" : true,
                "accepted_payment_methods": [
                    "credit_card",
                    "debit_card",
                    "pix"
                ],
                "credit_card": {
                  "operation_type": "auth_and_capture",
               },
                "Pix": {
                    "expires_in": 86400
                },
                "boleto": {
                    "instructions": "Pagar até o vencimento",
                    "due_at": "' . date('Y-m-d', strtotime('+7 days')) . '"
                }
            }
        }
    ]
}',
    'headers' => [
        'Accept' => 'application/json',
        'Authorization' => 'Basic ' . $authorization,
        'Content-Type' => 'application/json',
    ],
]);

$resposta = json_decode($response->getBody(), true);

echo ($resposta['checkouts'][0]['payment_url']);


// // Exemplo do Assinatura
// $response = $client->request('POST', 'https://api.pagar.me/core/v5/subscriptions/', [
//   'body' => '{"payment_method":"credit_card","interval":"month","minimum_price":10000,"interval_count":3,"billing_type":"prepaid","installments":3,"customer":{"document":"06444485983","phones":{"home_phone":{"country_code":"55","area_code":"49","number":"984198753"}},"name":"Tony Stark","email":"tonystark@avengers.com","type":"individual","document_type":"CPF","gender":"male"},"discounts":[{"cycles":3,"value":10,"discount_type":"percentage"}],"increments":[{"cycles":2,"value":20,"discount_type":"percentage"}],"items":[{"description":"Musculação","quantity":1,"pricing_scheme":{"price":18990}},{"description":"Matrícula","quantity":1,"cycles":1,"pricing_scheme":{"price":5990}}],"metadata":{"id":"my_subscription_id"},"currency":"BRL","boleto_due_days":5,"card":{"holder_name":"Tony Stark","number":"4532464862385322","exp_month":1,"exp_year":30,"cvv":"903","billing_address":{"line_1":"375, Av. General Justo, Centro","line_2":"8º andar","zip_code":"20021130","city":"Rio de Janeiro","state":"RJ","country":"BR"}}}',
//   'headers' => [
//     'Accept' => 'application/json',
//     'Authorization' => 'Basic '.$authorization,
//     'Content-Type' => 'application/json',
//   ],
// ]);

// echo $response->getBody();