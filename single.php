<?php get_header(); ?>
<section>
    <div class="pt-5 pb-3">
        <?php include get_template_directory() . '/component/breadcrumbs.php' ?>
    </div>
</section>
<section id="projeto">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 mx-auto">
                <h1 class="font-25 text-blue-dark-3 mb-3 mt-5 text-center"><?= the_title() ?></h1>
                <div class="img-featured py-4">
                    <?php the_post_thumbnail([960, 480]) ?>
                </div>
                <p><?php the_field('descricao'); ?></p>
                <div class="mb-5 pb-4">
                    <?php if (get_field('arquivo')) : ?>
                        <a class="more-info" target="_blank" href="<?php the_field('arquivo'); ?>">
                            <button>Ver informações do projeto</button>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <?php include get_template_directory() . '/component/projeto.php' ?>
</section>
<?php get_footer() ?>