<?php

// Habilita a imagem desetaque do post
add_theme_support('post-thumbnails');
//Fim

// Funções para Limpar o Header
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');
//Fim

//Fução para customizar a Pag Login
function my_custom_login_stylesheet()
{
    wp_enqueue_style('custom-login', get_template_directory_uri() . '/style.css');
}

add_action('login_enqueue_scripts', 'my_custom_login_stylesheet');
//Fim

function wpdocs_after_setup_theme()
{
    add_theme_support('html5', array('search-form'));
}
add_action('after_setup_theme', 'wpdocs_after_setup_theme');


// #
// #
// Registrar Variaveis Globais JS
function myplugin_ajaxurl()
{
    wp_enqueue_script('ajax-script', get_template_directory_uri() .
        '/js/main.js', array('jquery'), false, true);

    wp_localize_script(
        'ajax-script',
        'ajax_params',
        array(
            'ajax_url' => admin_url('admin-ajax.php'),
            'home_url' => home_url(),
            'theme_url' => get_stylesheet_directory_uri(),
        )
    );
}
add_action('wp_enqueue_scripts', 'myplugin_ajaxurl');

// #
// #
// ENVIAR CONTATO
add_action("wp_ajax_nopriv_save_doador", "save_doador");
add_action("wp_ajax_save_doador", "save_doador");
function save_doador()
{

    $data = array(
        "nome" => sanitize_text_field($_POST['nome']),
        "email" => sanitize_email($_POST['email']),
        "telefone" => sanitize_text_field($_POST['phone']),
        "projeto" => sanitize_text_field($_POST['projeto']),
        "valor" => sanitize_text_field($_POST['valor']),
        "lgpd" => sanitize_text_field($_POST['lgpd']),
    );
    global $wpdb;
    $wpdb->insert(
        'pdalvf_doador',
        array(
            'nome' => $data["nome"],
            'email' => $data["email"],
            'telefone' => $data["telefone"],
            'projeto' => $data["projeto"],
            'valor' => $data["valor"],
            'aceito_lgpd' => $data["lgpd"],
        )
    );
    echo "Salvo no Banco de dados";
    die();
}

// 
// 
// CAPTURAR DADOS PAGAR.ME
add_action("wp_ajax_nopriv_pagarme_capture", "pagarme_capture");
add_action("wp_ajax_pagarme_capture", "pagarme_capture");
function pagarme_capture()
{

    include __DIR__ . '/doacao/def.php';
    // echo $_POST;
    die();
}


// Custom Post Type
function custom_post_type_doacoes()
{

    $labels = [
        "name" => __("doacoes", "alvf.com.br"),
        "singular_name" => __("doacoes", "alvf.com.br"),
        "menu_name" => __("Meus doacoes", "alvf.com.br"),
        "all_items" => __("Todos os doacoes", "alvf.com.br"),
        "add_new" => __("Adicionar novo", "alvf.com.br"),
        "add_new_item" => __("Adicionar novo doacoes", "alvf.com.br"),
        "edit_item" => __("Editar doacoes", "alvf.com.br"),
        "new_item" => __("Novo doacoes", "alvf.com.br"),
        "view_item" => __("Ver doacoes", "alvf.com.br"),
        "view_items" => __("Ver doacoes", "alvf.com.br"),
        "search_items" => __("Pesquisar doacoes", "alvf.com.br"),
        "not_found" => __("Nenhum doacoes encontrado", "alvf.com.br"),
        "not_found_in_trash" => __("Nenhum doacoes encontrado na lixeira", "alvf.com.br"),
        "parent" => __("doacoes ascendente:", "alvf.com.br"),
        "featured_image" => __("Imagem destacada para esse doacoes", "alvf.com.br"),
        "set_featured_image" => __("Definir imagem destacada para esse doacoes", "alvf.com.br"),
        "remove_featured_image" => __("Remover imagem destacada para esse doacoes", "alvf.com.br"),
        "use_featured_image" => __("Usar como imagem destacada para esse doacoes", "alvf.com.br"),
        "archives" => __("Arquivos de doacoes", "alvf.com.br"),
        "insert_into_item" => __("Inserir no doacoes", "alvf.com.br"),
        "uploaded_to_this_item" => __("Enviar para esse doacoes", "alvf.com.br"),
        "filter_items_list" => __("Filtrar lista de doacoes", "alvf.com.br"),
        "items_list_navigation" => __("Navegação na lista de doacoes", "alvf.com.br"),
        "items_list" => __("Lista de doacoes", "alvf.com.br"),
        "attributes" => __("Atributos de doacoes", "alvf.com.br"),
        "name_admin_bar" => __("doacoes", "alvf.com.br"),
        "item_published" => __("doacoes publicado", "alvf.com.br"),
        "item_published_privately" => __("doacoes publicado de forma privada.", "alvf.com.br"),
        "item_reverted_to_draft" => __("doacoes revertido para rascunho.", "alvf.com.br"),
        "item_scheduled" => __("doacoes agendado", "alvf.com.br"),
        "item_updated" => __("doacoes atualizado.", "alvf.com.br"),
        "parent_item_colon" => __("doacoes ascendente:", "alvf.com.br"),
    ];

    $args = [
        "label" => __("doacoes", "alvf.com.br"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => ["slug" => "doacoes_", "with_front" => true],
        "query_var" => true,
        "supports" => ["title", "editor", "thumbnail"],
        "show_in_graphql" => false,
    ];

    register_post_type('doacoes', $args);
}
add_action('init', 'custom_post_type_doacoes');


// Fim

// REMOVE PERMISSÃO E OS ITENS DO MENU WP
// $user_id = get_current_user_id();
// if ($user_id > 1) { // 1 é o ID do usuário que deseja remover os itens do menu
//     function remove_menus()
//     {
//         // remove_menu_page('index.php'); //Dashboard 
//         remove_menu_page('edit.php'); //Posts - publicações
//         // remove_menu_page('upload.php'); //Media - imagens, vídeos, docs, etc...
//         // remove_menu_page('edit.php?post_type=page'); //Pages - páginas
//         remove_menu_page('edit-comments.php'); //Comments - comentários
//         // remove_menu_page('themes.php'); //Appearance - aparência (recomendo!)
//         // remove_menu_page('plugins.php'); //Plugins (recomendo!)
//         // remove_menu_page('users.php'); //Users - usuários 
//         remove_menu_page('tools.php'); //Tools - ferramentas (recomendo!)
//         // remove_menu_page('options-general.php'); //Settings - configurações 
//         // remove_menu_page('edit.php?post_type=acf') // ACF 
//     }
//     add_action('admin_menu', 'remove_menus');
// }
// if ($user_id == 1) { // 1 é o ID do usuário que deseja remover os itens do menu
//     function remove_menus()
//     {
//         // remove_menu_page('index.php'); //Dashboard 
//         remove_menu_page('edit.php'); //Posts - publicações
//         // remove_menu_page('upload.php'); //Media - imagens, vídeos, docs, etc...
//         // remove_menu_page('edit.php?post_type=page'); //Pages - páginas
//         remove_menu_page('edit-comments.php'); //Comments - comentários
//         remove_menu_page('themes.php'); //Appearance - aparência (recomendo!)
//         // remove_menu_page('plugins.php'); //Plugins (recomendo!)
//         // remove_menu_page('users.php'); //Users - usuários 
//         remove_menu_page('tools.php'); //Tools - ferramentas (recomendo!)
//         // remove_menu_page('options-general.php'); //Settings - configurações 
//         // remove_menu_page('edit.php?post_type=acf') // ACF 
//     }
//     add_action('admin_menu', 'remove_menus');
// }
// Fim