<div class="projetos">
    <?php $recebendo = new WP_Query(array(
        'post_type' => 'doacoes',
        'posts_per_page' => -1,
        'orderby' => 'date',
        'order' => 'DESC',
        'tax_query' => array(
            'relation' => 'E',
            array(
                'taxonomy' => 'situacao',
                'field' => 'slug',
                'terms' => 'incentivada',
            ),
        )
    ));
    while ($recebendo->have_posts()) : $recebendo->the_post();
        if (!has_term('conlcuido', 'situacao', get_the_ID())) : ?>
            <div class="card text-center">
                <div class="">
                    <a href="<?php the_permalink(); ?>">
                        <?php the_post_thumbnail([768, 451]) ?>
                        <svg id="Camada_1" data-name="Camada 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 13" width="20px">
                            <circle cx="6.5" cy="6.5" r="6.5" style="fill: #80acdc"></circle>
                            <polyline points="5.22 3.39 8.33 6.5 5.21 9.61" style="fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.28299336886953px"></polyline>
                        </svg>
                    </a>
                </div>
                <div>
                    <a href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>
                </div>
            </div>
    <?php endif;
    endwhile; ?>
</div>