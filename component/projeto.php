<?php
$lgpd = "Estou de acordo com a Lei Geral de Proteção de Dados (lei nº 13.709, de 14 de agosto de 2018) que dispõe sobre a proteção de dados pessoais. A ALVF, com o objetivo de cumprir com a legislação vigente sobre a matéria e proteger seus dados, solicita a leitura atenta da política institucional de proteção de dados e termos de uso de nossas páginas na internet, bem como o tratamento que será dado a sua estada em nosso ambiente digital.";
$projetosDoacao = new WP_Query([
    'post_type' => 'doacoes', 'posts_per_page' => -1, 'orderby' => 'title',
    'order' => 'ASC',
]);
?>
<div id="FORM_PROJETOS">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-7 mx-auto my-5">
                <div>

                    <h2>FAÇA SUA DOAÇÃO!</h2>
                    <p>Escolha o projeto que mais toca no seu coração e colha os frutos <br class="d-none d-lg-block">da generosidade, da empatia e do amor.</p>
                </div>
                <form enctype="multipart/form-data" method="post" id="form-projetoDoacao">
                    <!-- # -->
                    <!-- AMOUNT -->
                    <div id="amount">
                        <h3>Doe quanto puder</h3>
                        <div class="row justify-content-center aligm-item-center  mx-auto">
                            <span class="col-12 col-lg-3 my-0 dezRS">
                                <input checked type="radio" name="amount" id="dezRS" value="R$10,00"> <label for="dezRS">R$ 10,00</label>
                            </span>
                            <span class="col-12 col-lg-3 my-0 vinteRS">
                                <input type="radio" name="amount" id="vinteRS" value="R$20,00">
                                <label for="vinteRS">R$ 20,00</label>
                            </span>
                            <span class="col-12 col-lg-3 my-0 trintaRS">
                                <input type="radio" name="amount" id="trintaRS" value="R$30,00">
                                <label for="trintaRS">R$ 30,00</label>
                            </span>
                            <span class="col-12 col-lg-3 my-0 customRS">
                                <input type="radio" id="customRS" name="amount">
                                <span>
                                    <label for="customRS">Personalizar</label>
                                    <input type="text" name="amount" id="amountCustom" placeholder="Valor">
                                </span>
                            </span>
                        </div>
                    </div>
                    <!-- # -->
                    <!-- PROJETOS -->
                    <div id="colection_projetos" class="">
                        <h3>Selecione o projeto que deseja doar</h3>
                        <div class="proj_grid">
                            <?php while ($projetosDoacao->have_posts()) : $projetosDoacao->the_post();
                                $post_ID = get_the_ID();
                                if (!has_term('conlcuido', 'situacao', $post_ID)) :
                                    if (PAGE_ID == 1) : ?>
                                        <div>
                                            <input value="<?php the_title(); ?>" type="radio" id="projeto_<?= $post_ID ?>" name="projetosDoacao">
                                            <label for="projeto_<?= $post_ID ?>"><?php the_title(); ?></label>
                                        </div>
                                    <?php endif;
                                    if (PAGE_ID == $post_ID) : ?>
                                        <div>
                                            <input checked value="<?php the_title(); ?>" type="radio" id="projeto_<?= $post_ID ?>" name="projetosDoacao">
                                            <label for="projeto_<?= $post_ID ?>"><?php the_title(); ?></label>
                                        </div>
                            <?php endif;
                                endif;
                            endwhile; ?>
                        </div>
                    </div>
                    <!-- # -->
                    <!-- CUSTOMER -->
                    <div id="customer" class="">
                        <h3>Dados pessoais</h3>
                        <!-- NOME -->
                        <div>
                            <input id="nome-completo" type="text" placeholder="Nome completo" name="name"><br>
                            <input type="text" name="email" id="email" type="email" placeholder="E-mail"><br>
                            <div class="row m-0">
                                <div class="px-0 pr-2 col-4">
                                    <input type="text" id="area_code" name="area_code" placeholder="Código de área">
                                </div>
                                <div class="px-0 pl-2 col-8">
                                    <input disabled id="telefone" name="telefone" type="text" placeholder="Telefone">
                                </div>
                            </div>
                            <!-- CPF / CNPJ -->
                            <div class="row m-0 align-items-center">
                                <label class="col-auto" for="document_cpf">
                                    <input type="radio" name="document_type" id="document_cpf" value="CPF">
                                    CPF</label>
                                <label class="col-auto" for="document_cnpj">
                                    <input type="radio" name="document_type" id="document_cnpj" value="CNPJ">
                                    CNPJ</label>
                                <input disabled type="text" class="col" name="docment_number" placeholder="Nº do documento" id="document_number">
                            </div>
                            <!-- GENEROS -->
                            <div class="row mx-0 my-2 align-items-center">
                                <span class="col-4 px-1">
                                    <p class="p-0 m-0">Gênero:</p>
                                </span>
                                <span class="col-4 px-1">
                                    <input type="radio" name="gender" id="gender_male" value="male">
                                    <label class="col-auto" for="gender_male"> Masculino</label>
                                </span>
                                <span class="col-4 px-1">
                                    <input type="radio" name="gender" id="gender_female" value="female">
                                    <label class="col-auto" for="gender_female"> Feminino</label>
                                </span>
                            </div>
                            <!-- # -->
                            <!-- LGPD -->
                            <div id="politicas" class="mt-3 mb-4">
                                <label for="lgpd">
                                    <p>As doações efetuadas, tanto por pessoa física quanto por pessoa jurídica, podem ser deduzidas do Imposto de Renda (IR) por ocasião da Declaração Anual. Para tanto, solicite seu recibo de doação pelo WhatsApp <a target="_black" href="https://api.whatsapp.com/send?phone=554933216512">(49) 3321.6512</a>
                                    </p>
                                    <h5>Política de privacidade</h5>
                                    <p>
                                        <?= $lgpd; ?>
                                    </p>
                                </label>
                            </div>
                            <input type="radio" name="lgdp" id="lgpd" value="<?= $lgpd; ?>">
                            <label for="lgpd">Eu aceito</label>
                            <!-- BOTÃO -->
                            <div class="my-3">
                                <button id="submit-button" type="submit" style="margin-bottom:20px">Doar &nbsp;
                                    <svg id="Camada_1" data-name="Camada 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 13" width="20px">
                                        <circle cx="6.5" cy="6.5" r="6.5" style="fill: #80acdc"></circle>
                                        <polyline points="5.22 3.39 8.33 6.5 5.21 9.61" style="fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.28299336886953px"></polyline>
                                    </svg>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>