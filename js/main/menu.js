(function() {
    var $;

    $ = jQuery;

    $.fn.flexNav = function(options) {
        var $nav, $top_nav_items, breakpoint, count, nav_percent, nav_width, resetMenu, resizer, settings, showMenu, toggle_selector, touch_selector;
        settings = $.extend({
            'animationSpeed': 250,
            'transitionOpacity': true,
            'buttonSelector': '.nav-icon',
            'hoverIntent': false,
            'hoverIntentTimeout': 150,
            'calcItemWidths': false,
            'hover': false
        }, options);
        
        $nav = $(".flexnav");
        $nav.addClass('with-js');

        if (settings.transitionOpacity === true) {
            $nav.addClass('opacity');
        }

        $nav.find("li").each(function() {
            if ($(this).has("ul").length) {
                return $(this).addClass("item-with-ul").find("ul");
            }
        });
        if (settings.calcItemWidths === true) {
            $top_nav_items = $nav.find('>li');
            count = $top_nav_items.length;
            nav_width = 100 / count;
            nav_percent = nav_width + "%";
        }
        if ($nav.data('breakpoint')) {
            breakpoint = $nav.data('breakpoint');
        }
        showMenu = function() {
            if ($nav.hasClass('lg-screen') === true && settings.hover === true) {
                if (settings.transitionOpacity === true) {
                    return $(this).find('>ul').addClass('flexnav-show').stop(true, true).animate({
                        height: ["toggle", "swing"],
                        opacity: "toggle"
                    }, settings.animationSpeed);
                } else {
                    return $(this).find('>ul').addClass('flexnav-show').stop(true, true).animate({
                        height: ["toggle", "swing"]
                    }, settings.animationSpeed);
                }
            }
        };
        resetMenu = function() {
            if ($nav.hasClass('lg-screen') === true && $(this).find('>ul').hasClass('flexnav-show') === true && settings.hover === true) {
                if (settings.transitionOpacity === true) {
                    return $(this).find('>ul').removeClass('flexnav-show').stop(true, true).animate({
                        height: ["toggle", "swing"],
                        opacity: "toggle"
                    }, settings.animationSpeed);
                } else {
                    return $(this).find('>ul').removeClass('flexnav-show').stop(true, true).animate({
                        height: ["toggle", "swing"]
                    }, settings.animationSpeed);
                }
            }
        };
        touch_selector = '.item-with-ul, ' + settings['buttonSelector'];
        // $(touch_selector).append('<span class="touch-button"><i class="navicon">&#9660;</i></span>');
        toggle_selector = settings['buttonSelector'];
        $(toggle_selector).on('click', function(e) {
            var $btnParent, $thisNav, bs;
            $(toggle_selector).toggleClass('active');
            e.preventDefault();
            e.stopPropagation();
            $btnParent = $(this).is(bs) ? $(this) : $(this).parent(bs);
            $thisNav = $nav;

            if (!$("body,html").hasClass("overflow-hidden")) {
                $("body,html").addClass("overflow-hidden");
                // $("#logo").addClass("changed");

            } else if ($("body,html").hasClass("overflow-hidden")) {
                $("body,html").removeClass("overflow-hidden");
                // var distanceFromTop = $(document).scrollTop();
                // var logoWidth = $("#logo img").width();

                // if (distanceFromTop < $("body > header").height())
                //     $("#logo").removeClass("changed");
            }
            $("body>header").toggleClass("active-menu");

            return $thisNav.toggleClass('flexnav-show');
        });
        $('.touch-button').on('click', function(e) {
            var $sub, $touchButton;
            $sub = $(this).parent('.item-with-ul').find('>ul');
            $touchButton = $(this).parent('.item-with-ul').find('>span.touch-button');
            if ($nav.hasClass('lg-screen') === true) {
                $(this).parent('.item-with-ul').siblings().find('ul.flexnav-show').removeClass('flexnav-show').hide();
            }
            if ($sub.hasClass('flexnav-show') === true) {
                $sub.removeClass('flexnav-show').slideUp(settings.animationSpeed);
                return $touchButton.removeClass('active');
            } else if ($sub.hasClass('flexnav-show') === false) {
                $sub.addClass('flexnav-show').slideDown(settings.animationSpeed);
                return $touchButton.addClass('active');
            }
        });
        $nav.find('.item-with-ul *').focus(function() {
            $(this).parent('.item-with-ul').parent().find(".open").not(this).removeClass("open").hide();
            return $(this).parent('.item-with-ul').find('>ul').addClass("open").show();
        });
        // resizer();
        // return $(window).on('resize', resizer);
    };

    $.fn.flexNav()
}).call(this);
