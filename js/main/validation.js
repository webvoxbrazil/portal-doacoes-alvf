"use strict";
jQuery.noConflict();
$ = jQuery;
//
// VERIFICADOR DE CPF
function validaCPF(strCPF) {
  let cpf = String(strCPF).replace(/[^\d]/g, "");
  let Soma, Resto, i;
  Soma = 0;

  // Retorna false se tiver numeros iguais
  if (cpf == /^(\d)\1{9}/) {
    console.log(cpf);
    return false;
  }
  // Retorna False se tiver menos que 11 numeros
  if (cpf.length !== 11) return false;

  // Verifica se o CPF é Valido
  for (i = 1; i <= 9; i++)
    Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(cpf.substring(9, 10))) return false;

  Soma = 0;
  for (i = 1; i <= 10; i++)
    Soma = Soma + parseInt(cpf.substring(i - 1, i)) * (12 - i);
  Resto = (Soma * 10) % 11;

  if (Resto == 10 || Resto == 11) Resto = 0;
  if (Resto != parseInt(cpf.substring(10, 11))) return false;
  return true;
}
//
// VERIFICADDOR CNPJ
function validaCNPJ(strCNPJ) {
  let cnpj = String(strCNPJ).replace(/[^\d]/g, "");
  let b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];
  let i, n;

  // Retorna False se tiver menos que 14 numeros
  if (cnpj.length !== 14) return false;

  // Retorna False se tiver numeros iguais
  if (cnpj == /^(\d)\1{10}/) return false;

  // Valida o primeiro Numero verificador
  for (i = 0, n = 0; i < 12; n += cnpj[i] * b[++i]);
  if (cnpj[12] != ((n %= 11) < 2 ? 0 : 11 - n)) return false;

  // Valida o Segundo Numero Verificador
  for (i = 0, n = 0; i <= 12; n += cnpj[i] * b[i++]);
  if (cnpj[13] != ((n %= 11) < 2 ? 0 : 11 - n)) return false;

  return true;
}

//
//  VALIDA DOCUMENTO CPF OU CNPJ
function validaDocument(doc, type) {
  let document = String(doc).replace(/\D/g, "");
  // console.log(document);
  if (type == "CPF") {
    if (validaCPF(document)) {
      return document;
    } else {
      alert("Digite um CPF valido");
    }
  } else {
    if (validaCNPJ(document)) {
      return document;
    } else {
      alert("Digite um CNPJ valido");
    }
  }
}

//
//
// VALIDATIN DATA FORM
function validaUserFormData(user) {
  let customer = [];
  for (let i = 0; i < user.length - 1; i) {
    let value = $(user[i]).val(); // pega o valor
    let checked = $(user[i]).prop("checked"); // pega o atributo checked (true / false)
    let name = user[i].getAttribute("name"); // pega o atributo name
    let type = user[i].getAttribute("type"); // pega o atributo type
    if (value != "" || name == "amount") {
      // verifica se os campos estão preenchidos - excessa amount (valor)
      if (
        (type == "radio" && checked && value != "on") || // verifica qual input radio está c/ checked e c/ value
        (type == "text" && value != "") // verifica se os input text estão com value
      ) {
        customer.push({ name, value }); // add os dados num array
      }
      i++; // se todos os campos estão preenchidos, add +1 no loop
    } else {
      window.localStorage.setItem("customeer", "");
      // alert("Dados inseridos inválidos. \n\rPor favor, preencha corretamente!");
      alert(
        "ATENÇÃO! \nPara continuar você precisa preencher todos os campos."
      );
      return false;
    }
  }
  window.localStorage.setItem("customeer", JSON.stringify(customer));
  console.log("dados preenchidos");
  return true;
}
