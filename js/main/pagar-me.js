"use strict";
jQuery.noConflict();
$ = jQuery;
//
//
// ENVIA A REQUEST PARA O PAGAR.ME
$("#form-projetoDoacao").submit(function (event) {
  event.preventDefault();
  if (!validaUserFormData(event.target)) return false;
  let data = JSON.parse(localStorage.getItem("customeer"));
  // let valor = data[0].value.replace('R$', "").replace('.', "").replace(',', ".");
  let valor = data[0].value.replace(/\D/g, "");
  let doc = data[7].value.replace(/\D/g, "");
  let customer = {
    amount: String(valor),
    project_name: String(data[1].value),
    project_id: $('input[name="projetosDoacao"]:checked')
      .attr("id")
      .replace(/\D/g, ""),
    name: String(data[2].value),
    email: String(data[3].value),
    phone_area_code: String(data[4].value).replace(/\D/g, ""),
    phone_number: String(data[5].value).replace(/\D/g, ""),
    document_type: String(data[6].value),
    document: validaDocument(doc, data[6].value),
    gender: String(data[8].value),
    // lgpd: String(data[9].value),
  };
  $.ajax({
    type: "POST",
    // processData: false,
    // contentType: false,
    url: ajax_params.ajax_url,
    data: {
      action: "pagarme_capture",
      data: customer,
    },
    beforeSend: function () {
      $("#loader").removeClass("hidden");
      $("#teste_pagarme").hide();
    },
    // complete: function () {
    // },
    success: function (data) {
      window.location.assign(data);
      $("#loader").addClass("hidden");
      console.log(data);
    },
    error: function (data) {
      console.log(data);
    },
  });
});
