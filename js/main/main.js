"use strict";
jQuery.noConflict();
$ = jQuery;

//MASK DATA FORMS
$(document).ready(function () {
  $("#amountCustom").maskMoney({
    allowNegative: false,
    affixesStay: true,
    prefix: "R$ ",
    thousands: ".",
    decimal: ",",
  });
  $('input[name="document_type"]').change(function (a) {
    let doc_type = $('input[name="document_type"]:checked').val();
    $("#document_number").prop("disabled", false);
    if (doc_type == "CPF") {
      $("#document_number").mask("000.000.000-00", { reverse: true });
    } else {
      $("#document_number").mask("00.000.000/0000-00", { reverse: true });
    }
  });

  $("#area_code").mask("00");
  $("#telefone").mask("0 0000-0000");
  $("#area_code").mouseover(function () {
    $("#telefone").prop("disabled", false);
  });
});
