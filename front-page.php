<?php get_header(); ?>
<section id="bg-ajude">
    <picture>
        <source media="(min-width: 1920px )" srcset="<?= get_template_directory_uri() ?>/images/alvf-adote-um-projeto.jpg" sizes="">
        <source media="(min-width: 992px )" srcset="<?= get_template_directory_uri() ?>/images/alvf-adote-um-projeto_1.jpg" sizes="">
        <source media="(min-width: 768px )" srcset="<?= get_template_directory_uri() ?>/images/alvf-adote-um-projeto_1.jpg" sizes="">
        <source media="(min-width: 1px )" srcset="<?= get_template_directory_uri() ?>/images/alvf-adote-um-projeto_3.jpg" sizes="">
        <img src="<?= get_template_directory_uri() ?>/images/alvf-adote-um-projeto_3.jpg" alt="">
    </picture>
    <div id="breadcumb">
        <?php include get_template_directory() . '/component/breadcrumbs.php' ?>
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <h1 class="font-25 text-blue-dark-3 mb-3 mt-lg-5">
                        Adote um projeto
                    </h1>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="ajude">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 mx-auto py-5 my-3 text-center">
                <div>
                    <h2 class="pb-4">O melhor de cada um se encontra nas ações beneficentes <br class="d-none d-lg-block">
                        que podemos realizar em nossas vidas.</h2>
                    <p>
                        Realizar uma ação beneficente desperta o melhor em cada um de nós. Por isso, a ALVF (Associação Hospitalar
                        Lenoir Vargas Ferreira) conta com diversos projetos para captação de recursos que são destinados para o Hospital
                        Regional do Oeste, Hospital da Criança e Hospital Nossa Senhora da Saúde, em Coronel Freitas.
                    </p>
                    <p>
                        Os projetos propostos pela ALVF têm como objetivo melhorar as condições técnicas na prestação dos serviços, bem
                        como promover os fatores que favorecem a estadia e a humanização de todo o processo assistencial ao paciente.
                    </p>
                    <p>
                        Nossos projetos são apresentados em duas modalidades. Veja a seguir.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="incentivada">
    <div class="container">
        <div class="row py-5 ">
            <div class="col-12 col-lg-8 mx-auto pt-4">
                <h2><small>PROJETOS DE</small>DOAÇÃO INCENTIVADA</h2>
                <p>
                    São aquelas doações feitas aos Fundos da Criança e do Adolescente (FIA) e ao Fundo do Idoso com
                    projetos previamente aprovados pelo poder público para serem abatidas na declaração de Imposto de
                    Renda de Pessoas Físicas e Jurídicas
                </p>
                <p>
                    A pessoa ou empresa que realizar doação incentivada deve solicitar seu recibo de doação pelo
                    WhatsApp (49) 3321-6512, a fim de que seja apropriado o valor ao projeto.
                </p>
            </div>
            <div class="col-12 pb-5">
                <?php include get_template_directory() . '/component/list-incentivada.php' ?>
            </div>
        </div>
    </div>
</section>
<section id="espontanea">
    <div class="container">
        <div class="row py-5">
            <div class="col-12 col-lg-8 mx-auto pt-5">
                <h2><small>PROJETOS DE</small>DOAÇÃO ESPONTÂNEA ESPECÍFICA</h2>
                <p>São aquelas doações feitas decorrentes de um ato altruísta desinteressado onde a pessoa realiza doação em dinheiro, alimentos, roupas, tempo e outros bens. São específicas para um determinado projeto ou para custear despesas diversas da instituição. A pessoa ou empresa que realizar doação espontânea deve solicitar seu recibo de doação pelo WhatsApp (49) 3321-6512.</p>
            </div>
            <div class="col-12 pb-5">
                <?php include get_template_directory() . '/component/list-espontanea.php' ?>
            </div>
            <div class="col-12 col-lg-8 mx-auto">
                <h2><small>Doar patrimônio, bens duráveis, bens de consumo</small></h2>
                <ul>
                    <li>A pessoa altruísta pode doar:</li>
                    <li>Patrimônio imóvel como terrenos, casas, apartamentos e outros.</li>
                    <li>Bens duráveis como carros, equipamentos, material de construção e outros.</li>
                    <li>Bens de consumo como alimentos, bebidas, tecidos, saneantes, materiais hospitalares, medicamentos e outros.</li>
                    <li>Para fazer a doação, a pessoa ou empresa deve entrar em contato com o <strong>Setor de Captação de Recursos </strong> onde os profissionais farão as orientações e registros necessários:</li>
                    <li>Telefone/WhatsApp: <a target="_black" href="https://api.whatsapp.com/send?phone=554933216512">(49) 3321.6512</a></li>
                    <li>E-mail: <a href="mailto:cap.recursos@hro.org.br">cap.recursos@hro.org.br</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<section>
    <?php include get_template_directory() . '/component/projeto.php' ?>
</section>
<section id="projetosConcluidos">
    <div class="container">
        <div class="row justify-content-center align-items-center">
            <div class="col-12 text-center py-5">
                <div>
                    <h2>PROJETOS CONCLUÍDOS</h2>
                    <h3>OBJETIVOS ALCANÇADOS COM O APOIO <br> DE QUEM TEM O AMOR PELA VIDA NO CORAÇÃO.</h3>
                    <p>Veja todos os projetos concluídos em prol da saúde de toda a região.</p>
                    <a style="pointer-events: none; opacity:0.5;" href="/concluidos/">
                        <button style="margin-bottom:20px">Acessar &nbsp;
                            <svg id="Camada_1" data-name="Camada 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 13" width="20px">
                                <circle cx="6.5" cy="6.5" r="6.5" style="fill: #80acdc"></circle>
                                <polyline points="5.22 3.39 8.33 6.5 5.21 9.61" style="fill:none;stroke:#fff;stroke-linecap:round;stroke-linejoin:round;stroke-width:1.28299336886953px"></polyline>
                            </svg>
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>