</main>
<footer class="text-center text-lg-left">
    <div class="position-relative">
        <div class="position-absolute left-0 w-100 h-100">
            <div class="container-fluid w-100 h-100">
                <div class="row h-100">
                    <div class="col-lg-5 bg-blue-dark-4 pt-5 pb-4"> </div>
                    <div class="col-lg-7 bg-blue-dark-5"> </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row position-relative">
                <div class="col-lg-5 bg-blue-dark-4 pt-5 pb-4">
                    <?php require __DIR__ . '/images/logo-alvf.svg' ?>
                </div>
                <div class="col-lg-7 bg-blue-dark-5 pl-lg-5 d-none d-lg-block pb-4">
                    <div class="row align-items-lg-end h-100">
                        <div class="col-lg-4">
                            <a href="https://hro.org.br/" target="_blank">
                                <?php require __DIR__ . '/images/logo-hro.svg' ?>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="https://www.facebook.com/Hospital-Nossa-Senhora-da-Sa%C3%BAde-Coronel-Freitas-SC-234027533921365" target="_blank">
                                <?php require __DIR__ . '/images/logo-hnss.svg' ?>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="https://www.facebook.com/hospitaldacriancahc" target="_blank">
                                <?php require __DIR__ . '/images/logo-hc.svg' ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="position-relative">
        <div class="position-absolute left-0 w-100 h-100">
            <div class="container-fluid w-100 h-100">
                <div class="row h-100">
                    <div class="col-lg-5 bg-blue-dark-3 py-4"> </div>
                    <div class="col-lg-7 bg-blue-dark py-5 pl-lg-5"> </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-5 bg-blue-dark-3 py-4">
                    <div class="row">
                        <div class="col-lg-6">
                            <ul>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/sobre">Sobre</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/gestao">Gestão</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/transparencia">Transparência</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/editais">Editais</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/ajude-os-hospitais">Ajude os Hospitais</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/blog">Blog</a>
                                </li>
                                <li>
                                    <a class="text-white" href="https://alvf.org.br/contato">Contato</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-6 mt-4 mt-lg-0">
                            <a class="text-white" href="tel:49 3321.6500">49 3321.6500</a>
                            <address class="text-white d-block mt-4">
                                Rua Florianópolis, 1448-E<br>
                                Santa Maria | Chapecó - SC<br>
                                CEP 89812-505
                            </address>
                            <p class="text-white mt-2">
                                CNPJ: 02.122.913/0001-06
                            </p>
                        </div>
                        <div class="col-12 text-blue-light-2 mt-5 d-none d-lg-block">
                            <p class="font-12 mb-0">
                                Desenvolvido por <a href="https://voxbrazil.com.br" target="_blank">Vox Brazil Comunicação</a> © 2021.
                            </p>
                            <p class="font-12 mb-0">Todos os direitos reservados</p>
                            <p class="font-12 mb-0">
                                <a href="https://alvf.org.br/politica-de-privacidade">Política de privacidade</a>.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 bg-blue-dark py-5 pl-lg-5">
                    <div class="row">
                        <div class="col-lg-4 mb-5 mb-lg-0">
                            <div class="row no-gutters justify-content-center justify-content-lg-start">

                                <div class="col-5 col-sm-3 d-block d-lg-none text-left mx-2 px-0">
                                    <a href="https://hro.org.br/" target="_blank">
                                        <?php include "images/logo-hro.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto mx-2 px-0">
                                    <a href="https://www.facebook.com/HospitalRegionalDoOeste/" target="_blank">
                                        <?php include "images/icon-facebook.svg" ?>
                                    </a>
                                </div>
                                <div class="col-auto mx-2 px-0">
                                    <a href="https://www.instagram.com/hospitalregionaldooeste/" target="_blank">
                                        <?php include "images/instagram.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto  mx-2 px-0">
                                    <a href="https://twitter.com/hro_chapeco" target="_blank">
                                        <?php include "images/icon-twitter.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto col-lg-12 mt-lg-2 mx-2 px-0">
                                    <a class="text-white" href="tel:49 3321.6500">
                                        49 3321.6500
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4 mb-5 mb-lg-0">
                            <div class="row justify-content-center justify-content-lg-start">

                                <div class="col-5 col-sm-3 pl-0 d-block d-lg-none text-left mx-2 px-0">
                                    <a href="https://www.facebook.com/Hospital-Nossa-Senhora-da-Sa%C3%BAde-Coronel-Freitas-SC-234027533921365" target="_blank">
                                        <?php include "images/logo-hnss.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto mx-2 px-0">
                                    <a href="https://www.facebook.com/Hospital-Nossa-Senhora-da-Sa%C3%BAde-Coronel-Freitas-SC-234027533921365" target="_blank">
                                        <?php include "images/icon-facebook.svg" ?>
                                    </a>
                                </div>
                                <div class="col-auto mx-2 px-0">
                                    <a href=" https://www.instagram.com/hospitalnsadasaude/ " target="_blank">
                                        <?php include "images/instagram.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto col-lg-12 mt-lg-2 mx-2 px-0">
                                    <a class="text-white" href="tel:49 3347.0368" target="_blank">
                                        49 3347.0368
                                    </a>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="row justify-content-center justify-content-lg-start">
                                <div class="col-5 col-sm-3 pl-0 d-block d-lg-none text-left mx-2 px-0">
                                    <a href="https://www.facebook.com/hospitaldacriancahc" target="_blank">
                                        <?php include "images/logo-hc.svg" ?>
                                    </a>
                                </div>
                                <div class="col-auto mx-2 px-0">
                                    <a href="https://www.facebook.com/hospitaldacriancahc" target="_blank">
                                        <?php include "images/icon-facebook.svg" ?>
                                    </a>
                                </div>
                                <div class="col-auto mx-2 px-0">
                                    <a href="https://www.instagram.com/hospitaldacriancacco/" target="_blank">
                                        <?php include "images/instagram.svg" ?>
                                    </a>
                                </div>

                                <div class="col-auto col-lg-12 mt-lg-2 mx-2 px-0">
                                    <a class="text-white" href="tel:49 2049.1700">
                                        49 2049.1700
                                    </a>
                                </div>
                            </div>
                        </div>


                        <div class="col-12 text-blue-light-2 mt-5 d-block d-lg-none">
                            <p class="font-12 mb-0">
                                Desenvolvido por <a href="https://voxbrazil.com.br" target="_blank">Vox Brazil Comunicação</a> © <?= date("Y") ?>.
                            </p>
                            <p class="font-12 mb-0">Todos os direitos reservados</p>


                        </div>
                    </div>
                </div>
            </div>
        </div>
</footer>
<script src="<?= __ROOT__ ?>/js/jquery.mask.min.js"></script>
<script src="<?= __ROOT__ ?>/js/jquery.maskMoney.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<?php wp_footer(); ?>
</body>

</html>